/*
 * Generated Mon Mar 09 03:02:14 PDT 2015
 */


		.section	".rodata"
		.align		4
.$$.intFmt:
		.asciz		"%d"
.$$.strFmt:
		.asciz		"%s"
.$$.strTF:
		.asciz		"false\0\0\0true"
.$$.strEndl:
		.asciz		"\n"
.$$.strArrBound:
		.asciz		"Index value of %d is outside legal range [0,%d).\n"
.$$.strNullPtr:
		.asciz		"Attempt to dereference NULL pointer.\n"

		.section	".text"
		.align		4
.$$.printBool:
		save		%sp, -96, %sp
		set		.$$.strTF, %o0
		cmp		%g0, %i0
		be		.$$.printBool2
		nop
		add		%o0, 8, %o0
.$$.printBool2:
		call		printf
		nop
		ret
		restore

.$$.arrCheck:
		save		%sp, -96, %sp
		cmp		%i0, %g0
		bl		.$$.arrCheck2
		nop
		cmp		%i0, %i1
		bge		.$$.arrCheck2
		nop
		ret
		restore
.$$.arrCheck2:
		set		.$$.strArrBound, %o0
		mov		%i0, %o1
		call		printf
		mov		%i1, %o2
		call		exit
		mov		1, %o0
		ret
		restore
.$$.ptrCheck:
		save		%sp, -96, %sp
		cmp		%i0, %g0
		bne		.$$.ptrCheck2
		nop
		set		.$$.strNullPtr, %o0
		call		printf
		nop
		call		exit
		mov		1, %o0
.$$.ptrCheck2:
		ret
		restore


.global		main
main:
	set			SAVE.main, %g1
	save		%sp, %g1, %sp
		
		! Store params
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(x<100)
	.$$.loopCheck.1:
			
			! x<100
			set			-4, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			set			100, %o1
			cmp			%o0, %o1
			bge			.$$.cmp.1
			mov			%g0, %o0
			inc			%o0
		.$$.cmp.1:
			set			-12, %o1
			add			%fp, %o1, %o1
			st			%o0, [%o1]
			
			! Check loop condition
			set			-12, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.1
			nop
			
			! Start of loop body
					
					! x++
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-16, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-4, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
			
			! End of loop body
			ba			.$$.loopCheck.1
			nop
		.$$.loopEnd.1:
		
		! cout << x: should be 100\t
		
		.section	".rodata"
		.align		4
	.$$.str.1:
		.asciz		"x: should be 100\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.1, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.2:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.2, %o1
		call		printf
		nop
		
		! x==100
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			100, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.2
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.2:
		set			-20, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==100
		set			-20, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(x>=7)
	.$$.loopCheck.2:
			
			! x>=7
			set			-4, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			set			-7, %o1
			cmp			%o0, %o1
			bl			.$$.cmp.3
			mov			%g0, %o0
			inc			%o0
		.$$.cmp.3:
			set			-24, %o1
			add			%fp, %o1, %o1
			st			%o0, [%o1]
			
			! Check loop condition
			set			-24, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.2
			nop
			
			! Start of loop body
					
					! x--
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					sub			%o0, %o1, %o2
					set			-28, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-4, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
			
			! End of loop body
			ba			.$$.loopCheck.2
			nop
		.$$.loopEnd.2:
		
		! cout << x: should be -8\t\t
		
		.section	".rodata"
		.align		4
	.$$.str.3:
		.asciz		"x: should be -8\t\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.3, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.4:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.4, %o1
		call		printf
		nop
		
		! x==8
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			-8, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.4
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.4:
		set			-32, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==8
		set			-32, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(true)
	.$$.loopCheck.3:
			
			! Check loop condition
			set			1, %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.3
			nop
			
			! Start of loop body
					
					! break
					ba			.$$.loopEnd.3
					nop
					
					! x++
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-36, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-4, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
			
			! End of loop body
			ba			.$$.loopCheck.3
			nop
		.$$.loopEnd.3:
		
		! cout << x: should be 0\t\t
		
		.section	".rodata"
		.align		4
	.$$.str.5:
		.asciz		"x: should be 0\t\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.5, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.6:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.6, %o1
		call		printf
		nop
		
		! x==0
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			0, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.5
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.5:
		set			-40, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==0
		set			-40, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(true)
	.$$.loopCheck.4:
			
			! Check loop condition
			set			1, %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.4
			nop
			
			! Start of loop body
					
					! x==3
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			3, %o1
					cmp			%o0, %o1
					bne			.$$.cmp.6
					mov			%g0, %o0
					inc			%o0
				.$$.cmp.6:
					set			-44, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					
					! if ( x==3 )
					set			-44, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					cmp			%o0, %g0
					be			.$$.else.1
					nop
						
						! break
						ba			.$$.loopEnd.4
						nop
						
						ba			.$$.endif.1
						nop
					
					! else
				.$$.else.1:
					
					! endif
				.$$.endif.1:
					
					! x++
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-48, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-4, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
			
			! End of loop body
			ba			.$$.loopCheck.4
			nop
		.$$.loopEnd.4:
		
		! cout << x: should be 3\t\t
		
		.section	".rodata"
		.align		4
	.$$.str.7:
		.asciz		"x: should be 3\t\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.7, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.8:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.8, %o1
		call		printf
		nop
		
		! x==3
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			3, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.7
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.7:
		set			-52, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==3
		set			-52, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(x==3)
	.$$.loopCheck.5:
			
			! x==3
			set			-4, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			set			3, %o1
			cmp			%o0, %o1
			bne			.$$.cmp.8
			mov			%g0, %o0
			inc			%o0
		.$$.cmp.8:
			set			-56, %o1
			add			%fp, %o1, %o1
			st			%o0, [%o1]
			
			! Check loop condition
			set			-56, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.5
			nop
			
			! Start of loop body
					
					! x++
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-60, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-4, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
			
			! End of loop body
			ba			.$$.loopCheck.5
			nop
		.$$.loopEnd.5:
		
		! cout << x: should be 0\t\t
		
		.section	".rodata"
		.align		4
	.$$.str.9:
		.asciz		"x: should be 0\t\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.9, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.10:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.10, %o1
		call		printf
		nop
		
		! x==0
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			0, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.9
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.9:
		set			-64, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==0
		set			-64, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! y = 0
		set			-8, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(y<=4)
	.$$.loopCheck.6:
			
			! y<=4
			set			-8, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			set			4, %o1
			cmp			%o0, %o1
			bg			.$$.cmp.10
			mov			%g0, %o0
			inc			%o0
		.$$.cmp.10:
			set			-68, %o1
			add			%fp, %o1, %o1
			st			%o0, [%o1]
			
			! Check loop condition
			set			-68, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.6
			nop
			
			! Start of loop body
					
					! y++
					set			-8, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-72, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-8, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
					
					! y!=3
					set			-8, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			3, %o1
					cmp			%o0, %o1
					be			.$$.cmp.11
					mov			%g0, %o0
					inc			%o0
				.$$.cmp.11:
					set			-76, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					
					! if ( y!=3 )
					set			-76, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					cmp			%o0, %g0
					be			.$$.else.2
					nop
						
						! continue
						ba			.$$.loopCheck.6
						nop
						
						ba			.$$.endif.2
						nop
					
					! else
				.$$.else.2:
					
					! endif
				.$$.endif.2:
					
					! x++
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-80, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-4, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
			
			! End of loop body
			ba			.$$.loopCheck.6
			nop
		.$$.loopEnd.6:
		
		! cout << x: should be 1\t\t
		
		.section	".rodata"
		.align		4
	.$$.str.11:
		.asciz		"x: should be 1\t\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.11, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.12:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.12, %o1
		call		printf
		nop
		
		! x==1
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			1, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.12
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.12:
		set			-84, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==1
		set			-84, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(true)
	.$$.loopCheck.7:
			
			! Check loop condition
			set			1, %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.7
			nop
			
			! Start of loop body
					
					! x+100
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			100, %o1
					add			%o0, %o1, %o0
					set			-88, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					
					! x = x+100
					set			-4, %o1
					add			%fp, %o1, %o1
					set			-88, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					st			%o0, [%o1]
					
					! while(true)
				.$$.loopCheck.8:
						
						! Check loop condition
						set			1, %o0
						cmp			%o0, %g0
						be			.$$.loopEnd.8
						nop
						
						! Start of loop body
								
								! x+10
								set			-4, %l7
								add			%fp, %l7, %l7
								ld			[%l7], %o0
								set			10, %o1
								add			%o0, %o1, %o0
								set			-92, %o1
								add			%fp, %o1, %o1
								st			%o0, [%o1]
								
								! x = x+10
								set			-4, %o1
								add			%fp, %o1, %o1
								set			-92, %l7
								add			%fp, %l7, %l7
								ld			[%l7], %o0
								st			%o0, [%o1]
								
								! while(true)
							.$$.loopCheck.9:
									
									! Check loop condition
									set			1, %o0
									cmp			%o0, %g0
									be			.$$.loopEnd.9
									nop
									
									! Start of loop body
											
											! x+1
											set			-4, %l7
											add			%fp, %l7, %l7
											ld			[%l7], %o0
											set			1, %o1
											add			%o0, %o1, %o0
											set			-96, %o1
											add			%fp, %o1, %o1
											st			%o0, [%o1]
											
											! x = x+1
											set			-4, %o1
											add			%fp, %o1, %o1
											set			-96, %l7
											add			%fp, %l7, %l7
											ld			[%l7], %o0
											st			%o0, [%o1]
											
											! break
											ba			.$$.loopEnd.9
											nop
									
									! End of loop body
									ba			.$$.loopCheck.9
									nop
								.$$.loopEnd.9:
								
								! break
								ba			.$$.loopEnd.8
								nop
						
						! End of loop body
						ba			.$$.loopCheck.8
						nop
					.$$.loopEnd.8:
					
					! break
					ba			.$$.loopEnd.7
					nop
			
			! End of loop body
			ba			.$$.loopCheck.7
			nop
		.$$.loopEnd.7:
		
		! cout << x: should be 111\t
		
		.section	".rodata"
		.align		4
	.$$.str.13:
		.asciz		"x: should be 111\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.13, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.14:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.14, %o1
		call		printf
		nop
		
		! x==111
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			111, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.13
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.13:
		set			-100, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==111
		set			-100, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
		
		! x = 0
		set			-4, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! y = 0
		set			-8, %o1
		add			%fp, %o1, %o1
		set			0, %o0
		st			%o0, [%o1]
		
		! while(y<3)
	.$$.loopCheck.10:
			
			! y<3
			set			-8, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			set			3, %o1
			cmp			%o0, %o1
			bge			.$$.cmp.14
			mov			%g0, %o0
			inc			%o0
		.$$.cmp.14:
			set			-104, %o1
			add			%fp, %o1, %o1
			st			%o0, [%o1]
			
			! Check loop condition
			set			-104, %l7
			add			%fp, %l7, %l7
			ld			[%l7], %o0
			cmp			%o0, %g0
			be			.$$.loopEnd.10
			nop
			
			! Start of loop body
					
					! y==1
					set			-8, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					cmp			%o0, %o1
					bne			.$$.cmp.15
					mov			%g0, %o0
					inc			%o0
				.$$.cmp.15:
					set			-108, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					
					! if ( y==1 )
					set			-108, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					cmp			%o0, %g0
					be			.$$.else.3
					nop
						
						! y = 3
						set			-8, %o1
						add			%fp, %o1, %o1
						set			3, %o0
						st			%o0, [%o1]
						
						! continue
						ba			.$$.loopCheck.10
						nop
						
						ba			.$$.endif.3
						nop
					
					! else
				.$$.else.3:
					
					! endif
				.$$.endif.3:
					
					! x++
					set			-4, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-112, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-4, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
					
					! y++
					set			-8, %l7
					add			%fp, %l7, %l7
					ld			[%l7], %o0
					set			1, %o1
					add			%o0, %o1, %o2
					set			-116, %o1
					add			%fp, %o1, %o1
					st			%o0, [%o1]
					set			-8, %o1
					add			%fp, %o1, %o1
					st			%o2, [%o1]
			
			! End of loop body
			ba			.$$.loopCheck.10
			nop
		.$$.loopEnd.10:
		
		! cout << x: should be 1\t\t
		
		.section	".rodata"
		.align		4
	.$$.str.15:
		.asciz		"x: should be 1\t\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.15, %o1
		call		printf
		nop
		
		! cout << x
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o1
		set			.$$.intFmt, %o0
		call		printf
		nop
		
		! cout << \t
		
		.section	".rodata"
		.align		4
	.$$.str.16:
		.asciz		"\t"
		
		.section	".text"
		.align		4
		set			.$$.strFmt, %o0
		set			.$$.str.16, %o1
		call		printf
		nop
		
		! x==1
		set			-4, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		set			1, %o1
		cmp			%o0, %o1
		bne			.$$.cmp.16
		mov			%g0, %o0
		inc			%o0
	.$$.cmp.16:
		set			-120, %o1
		add			%fp, %o1, %o1
		st			%o0, [%o1]
		
		! cout << x==1
		set			-120, %l7
		add			%fp, %l7, %l7
		ld			[%l7], %o0
		call		.$$.printBool
		nop
		
		! cout << endl
		set			.$$.strEndl, %o0
		call		printf
		nop
	
	!  End of function main
	call		main.fini
	nop
	ret
	restore
	SAVE.main = -(92 + 120) & -8
	
main.fini:
	save		%sp, -96, %sp
	ret
	restore
