# CSE 131 Tests - Winter 2015 #

##To Run##
I usually run:

```
#!bash
yuno run phase # check #
or 
yuno run phase #
```
assuming you have the right symlink set up:

```
#!bash
alias yuno='python /Users/username/cse131/yuno/yuno.py'
```

##Project folder structure##

```
#!bash

cse131/
    RC
    RCdbg
    bin/
        *.class
    lib/
        java-cup-v11a.jar
        javacup
    src/
        *.java
    cse131tests/
        project1Testcases/
            phase#/
                check#/
                      *.rc
                      *.ans.out
        project2Testcases/
    yuno/
        setting/
```


##How to set up YUNO##
Copy the config.json file into your yuno/settings folder

##Note##
Tests are from various classes so some tests are testing things our class isn't doing